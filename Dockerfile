FROM openjdk:8-jdk-slim

LABEL maintainer="wenige@infai.org"

#ENV ARTIFACT=train_1
ENV CATALOG_URL="https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl"

RUN apt-get update \
  && apt-get upgrade -y \
  && mkdir -p /usr/share/man/man1mkdir -p /usr/share/man/man1 \
  && apt-get install maven git sudo wget jq -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && git clone https://github.com/SmartDataAnalytics/SparqlIntegrate.git \
  && cd SparqlIntegrate \
  && git checkout 15232bb \
  && mvn clean install -DskipTests=true \  
  && ./reinstall-debs.sh

COPY . /app
WORKDIR /app
RUN ./load-data.sh

FROM tenforce/virtuoso:latest
COPY --from=0 /app/toLoad toLoad

