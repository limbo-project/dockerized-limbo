#!/bin/bash
echo $ARTIFACT
wget $CATALOG_URL
[ -d toLoad ] || mkdir toLoad
URLs=`sparql-integrate --wig-pretty prefixes.ttl catalog.all.ttl download-rules/*.sparql --jq | jq -r '.[].url.id'`
for URL in $URLs
do
    wget $URL -P toLoad
done


