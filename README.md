# Dockerized-limbo

With this docker image, you can load LIMBO data into a Virtuoso server running in a Docker environment. Prior to setting up the server, the image:

* gets the [LIMBO metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl)

* queries the latest LIMBO datasets and the respective download links based on the properties `owl:priorVersion` and `owl:versionInfo`

* downloads the .ttl files into the `toLoad` folder Virtuosos initial bulk load procedure

The image is based on the [tenforce/virtusoso](https://hub.docker.com/r/tenforce/virtuoso/) docker image.

## Build the image and run the container


*  `docker build . -t limbo:latest`

*  `docker run -p 8890:8890 limbo:latest`

## Load a specific dataset only

You can also restrict the loading to a specific LIMBO dataset (e.g., [train_1](https://gitlab.com/limbo-project/train_1-dataset)). Just set the environment variable `ARTIFACT` to the dataset identifier (`dataid:artifact`) of the [LIMBO metadata-catalog](https://gitlab.com/limbo-project/metadata-catalog/raw/master/catalog.all.ttl).

### Example
`docker run -p 8890:8890  -e ARTIFACT=train_1 limbo:latest`



